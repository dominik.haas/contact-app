package contactList.appClasses;

public class Contact {
	
//	● Your program maintains a TreeSet of contacts
//	– The “equals” and “compareTo” methods are essential
//	● Your program implements the data class “Contact”
//	–Allow zero or more phone numbers (flexible number)
//	–Allow zero or more email addresses (flexible number

	private String name;
	private String prename;
	private int telNumber;
	private String emailAdress;
	
	public Contact(String name, String prename) {
		this.name = name;
		this.prename = prename;
	}
	
	public String toString() {
		return ""+name+" "+prename+" "+telNumber+" "+emailAdress;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPrename() {
		return prename;
	}
	
	public void setPrename(String prename) {
		this.prename = prename;
	}
	
	public int getTelNumber() {
		return telNumber;
	}
	public void setTelNumber(int telNumber) {
		this.telNumber = telNumber;
	}
	
	public String getEmailAdress() {
		return emailAdress;
	}
	public void setEmailAdress(String emailAdress) {
		this.emailAdress = emailAdress;
	}
	
	
}
