package contactList;

import contactList.appClasses.App_Controller;
import contactList.appClasses.App_Model;
import contactList.appClasses.App_View;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class JavaFX_App_Template extends Application {
	
    private static JavaFX_App_Template mainProgram; // singleton

    private App_View view;
    private App_Model model;
    private App_Controller controller;

    private ServiceLocator serviceLocator; // resources, after initialization

    public static void main(String[] args) {
        launch(args);
    }

    
    @Override
    public void init() {
        if (mainProgram == null) {
            mainProgram = this;
        } else {
            Platform.exit();
        }
    }

   
    @Override
    public void start(Stage stage) {	
		model = new App_Model();
		view = new App_View(stage, model);
		controller = new App_Controller(model, view);
		view.start();
	}

}