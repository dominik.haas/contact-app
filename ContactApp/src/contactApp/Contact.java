package contactApp;

import java.util.ArrayList;

public class Contact implements Comparable<Contact> {
	
	private String name;
	private String prename;
	private ArrayList<String> telArr = new ArrayList<String>();
	private ArrayList<String> emailArr = new ArrayList<String>();
	
	public Contact(String prename, String name) {
		this.prename = prename;
		this.name = name;
	}
	
	public String toString() {
		return ""+name+" "+prename+" "+telArr+" "+emailArr;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPrename() {
		return prename;
	}
	
	public void setPrename(String prename) {
		this.prename = prename;
	}
	
	public int compareTo(Contact o) {
		// TODO Auto-generated method stub
		return this.name.compareTo(o.name);
	}
	
	public ArrayList<String> getTelArr() {
		return telArr;
		
	}
	
	public void addTelToArray (String t) {
		telArr.add(t);
	}
	
	public String getTelFromArray () {
		for (int i = 0; i < telArr.size(); i++) {
			String s = telArr.get(i);
			return s;
		}
		return null;
	}
	
	public ArrayList<String> getEmailArr() {
		return emailArr;
		
	}
	
	public void addEmailToArray (String t) {
		emailArr.add(t);
	}
	
	public String getEmailFromArray () {
		 for (String s : emailArr) { 		      
	           return s.toString(); 		
	      }
		return null;
	}
}
