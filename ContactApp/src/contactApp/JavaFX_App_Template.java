package contactApp;

import contactApp.appClasses.App_Controller;
import contactApp.appClasses.App_Model;
import contactApp.appClasses.App_View;
import contactApp.splashScreen.Splash_Controller;
import contactApp.splashScreen.Splash_Model;
import contactApp.splashScreen.Splash_View;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;


public class JavaFX_App_Template extends Application {
    private static JavaFX_App_Template mainProgram; // singleton
    private Splash_View splashView;
    private App_View view;
    private App_Model model;

    private ServiceLocator serviceLocator; // resources, after initialization

    public static void main(String[] args) {
    	
    	launch(args);
    	
    }

 
    @Override
    public void init() {
        if (mainProgram == null) {
            mainProgram = this;
        } else {
            Platform.exit();
        }
    }

     @Override
    public void start(Stage primaryStage) {
        // Create and display the splash screen and model
        Splash_Model splashModel = new Splash_Model();
        splashView = new Splash_View(primaryStage, splashModel);
        new Splash_Controller(this, splashModel, splashView);
        splashView.start();

        // Display the splash screen and begin the initialization
        splashModel.initialize();
    }

     public void startApp() {
        Stage appStage = new Stage();

        // Initialize the application MVC components. Note that these components
        // can only be initialized now, because they may depend on the
        // resources initialized by the splash screen
        App_Model model = new App_Model();
        view = new App_View(appStage, model);
        new App_Controller(model, view);

        // Resources are now initialized
        serviceLocator = ServiceLocator.getServiceLocator();

        // Close the splash screen, and set the reference to null, so that all
        // Splash_XXX objects can be garbage collected
        splashView.stop();
        splashView = null;

        view.start();
    }

    @Override
    public void stop() {
        serviceLocator.getConfiguration().save();
        if (view != null) {
            // Make the view invisible
            view.stop();
        }

        // More cleanup code as needed

        serviceLocator.getLogger().info("Application terminated");
    }

    // Static getter for a reference to the main program object
    protected static JavaFX_App_Template getMainProgram() {
        return mainProgram;
    }
}
