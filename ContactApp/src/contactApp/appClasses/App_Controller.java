package contactApp.appClasses;

import java.util.Iterator;

import contactApp.Contact;
import contactApp.ServiceLocator;
import contactApp.abstractClasses.Controller;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.stage.WindowEvent;


public class App_Controller extends Controller<App_Model, App_View> {
    ServiceLocator serviceLocator;

    public App_Controller(App_Model model, App_View view) {
        super(model, view);
        view.getStage().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
            }
        });
        
        serviceLocator = ServiceLocator.getServiceLocator();        
        serviceLocator.getLogger().info("Application controller initialized");
        
        //Plus Button
        view.create.setOnAction(e -> {
        	view.pretxt.setText("");
        	view.nametxt.setText("");
        	view.emailtxt.setText("");
        	view.teltxt.setText("");
        	view.gp.getChildren().clear();
        	view.secondScene.setRoot(view.gp);
        	view.secondScene.getRoot();
        	view.createSecondGUI();
        	
        	serviceLocator.getLogger().info("Open create new contact");
        });
        
        
        //Create new Contact
        view.createContact.setOnAction(e -> {
        	
        	Contact c1 = new Contact(view.pretxt.getText(), view.nametxt.getText());
        	
        	if(emailValid(view.emailtxt.getText()) == true) {
        		if(telValid(view.teltxt.getText()) == true) {
	
        		String[] x = view.emailtxt.getText().split(",");
        		
        		for(String q : x) {
        			c1.addEmailToArray(q);	
        		}
        		
        		String[] z = view.teltxt.getText().split(",");
        		
        		for(String v : z) {
        			c1.addTelToArray(v);	
        		}	
        		
        		c1.addTelToArray(view.teltxt.getText());
        		model.contactList.add(c1);
        		view.tv.getItems().add(c1.getPrename());
        		
        	} else {
        		serviceLocator.getLogger().info("Mail or phone number has a wrong format");
        	}
        	
        	}	
        });

        //save changes
        view.saveChanges.setOnAction(e -> {
        	
        	String c = App_View.tv.getSelectionModel().getSelectedItem();
        	
        	for(Contact z : App_Model.contactList) {
         		if(z.getPrename() == c) {
         			
         			
         			z.setPrename(view.pretxt.getText());
         			z.setName(view.nametxt.getText());
         			z.getEmailArr().clear();
         			z.getTelArr().clear();
         			
         			
         			
         			for(int i = 0; i < view.txtArrEmail.size(); i++) {
         				z.addEmailToArray(view.txtArrEmail.get(i).getText());
         			}
         			for(int i = 0; i < view.txtArrTel.size(); i++) {
         				z.addTelToArray(view.txtArrTel.get(i).getText());
         			}
         		}
         	}
        
        	view.tv.getItems().clear();
        	for(Contact z : App_Model.contactList) {
            	view.tv.getItems().add(z.getPrename());
            }
        	serviceLocator.getLogger().info("Contact changed");
        	
        	
        });
        
        //double click on contact
        App_View.tv.setOnMouseClicked(e ->  {
        	    if(e.getButton().equals(MouseButton.PRIMARY)){
                    if(e.getClickCount() == 2){
                    	view.vb.getChildren().clear();
                    	view.thirdScene.setRoot(view.vb);
                    	view.thirdScene.getRoot();
                    	view.createThirdGUI();
                    
                    	view.pretxt.setText(view.tv.getSelectionModel().getSelectedItem()); 
                    	 
                    	String c = App_View.tv.getSelectionModel().getSelectedItem();
                    	
                     	for(Contact z : App_Model.contactList) {
                     		if(z.getPrename() == c) {
                     			view.nametxt.setText(z.getName());		
                     		}
                     	}
                     	
                     	for(Contact z : App_Model.contactList) {
                     		if(z.getPrename() == c) {
                     			for(String x : z.getEmailArr()) {
                     				view.emailtxt.setText(x);
                     			}
                     			
                     			view.emailtxt.setText(z.getEmailFromArray());
                     		}		
                     	}
                     	
                     	for(Contact z : App_Model.contactList) {
                     		if(z.getPrename() == c) {
                     			view.teltxt.setText(z.getTelFromArray());
                     		}		
                     	}
                     	
                     	
                       serviceLocator.getLogger().info("Open contact");
                    }
                }
        });

        //Minus button
        view.delete.setOnAction(e -> {
        	String c = App_View.tv.getSelectionModel().getSelectedItem();

        		for (Iterator iterator = model.contactList.iterator(); iterator.hasNext();) {
        		     Contact s = (Contact) iterator.next();
        		     if(s.getPrename() == c) {
        		     iterator.remove();
        		     }
        		}
        	view.tv.getItems().clear();
        	for(Contact z : App_Model.contactList) {
            	view.tv.getItems().add(z.getPrename());
            }
        });  
    }  
    //Email Validation
    public boolean emailValid (String s ) {
    	if(s.contains("@")) {
    		return true;
    	} else {
    		return false;
    	}
    	
    }
    //Email Validation
    public static boolean telValid(String str) { 
    	  try {  
    	    Double.parseDouble(str);  
    	    return true;
    	  } catch(NumberFormatException e){  
    	    return false;  
    	  }  
    	}
}