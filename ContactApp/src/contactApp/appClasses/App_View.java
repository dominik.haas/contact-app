package contactApp.appClasses;

import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Logger;

import contactApp.Contact;
import contactApp.ServiceLocator;
import contactApp.abstractClasses.View;
import contactApp.commonClasses.Translator;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;


public class App_View extends View<App_Model> {
	
	
	Menu menuFile;
    Menu menuFileLanguage;
    Menu menuHelp;
    
    Label lblNumber;
    Button btnClick;
    Button create;
    Button delete;
    Button createContact = new Button();
    Label addEmail = new Label();
    Label addNumber = new Label();
    Button saveChanges = new Button();
    
    
    TextField pretxt = new TextField();
	TextField nametxt = new TextField();
	TextField emailtxt = new TextField();
	TextField teltxt = new TextField();
	
	Label prelbl = new Label();
	Label namelbl = new Label();
	Label emaillbl = new Label();
	Label tellbl = new Label();
	Label labeli = new Label();
	
	GridPane gp = new GridPane();
	VBox vb = new VBox();
	Scene thirdScene = new Scene(vb, 400, 400);
	Scene secondScene = new Scene(gp, 500, 200);
	Stage newWindow = new Stage();
	
	ArrayList<TextField> txtArrEmail = new ArrayList<>();
	ArrayList<TextField> txtArrTel = new ArrayList<>();
    
    static ListView<String> tv = new ListView<String>();

    public App_View(Stage stage, App_Model model) {
        super(stage, model);
        ServiceLocator.getServiceLocator().getLogger().info("Application view initialized");
        updateTexts();
    }
    
    protected Scene create_GUI() {
	
    	this.stage = stage;
    	
	    ServiceLocator sl = ServiceLocator.getServiceLocator();  
	    Logger logger = sl.getLogger();
	    
	    MenuBar menuBar = new MenuBar();
	    menuFile = new Menu();
	    menuFileLanguage = new Menu();
	    menuFile.getItems().add(menuFileLanguage);
	    
       for (Locale locale : sl.getLocales()) {
           MenuItem language = new MenuItem(locale.getLanguage());
           menuFileLanguage.getItems().add(language);
           language.setOnAction( event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
                sl.setTranslator(new Translator(locale.getLanguage()));
                updateTexts();
            });
        }
       
        App_Model.addContacts();
        tv.setMaxWidth(225);
        for(Contact c : model.contactList) {
        	tv.getItems().add(c.getPrename());
        }
        
        menuHelp = new Menu();
	    menuBar.getMenus().addAll(menuFile, menuHelp);
	    
	    create = new Button("+");
	    create.setShape(new Circle(1.5));
	    create.setMinSize(50,50);
	    
	    delete = new Button("-");
	    delete.setShape(new Circle(1.5));
	    delete.setMinSize(50,50);
	    
	    BorderPane rightSide = new BorderPane();

	    rightSide.setTop(create);
	    rightSide.setBottom(delete);
	    
	    
		BorderPane root = new BorderPane();
		root.setMinHeight(500);
		root.setMinWidth(500);
		
		root.setTop(menuBar);
        root.setRight(rightSide);
        root.setLeft(labeli);
        root.setCenter(tv);
       
		
        Scene scene = new Scene(root);
        scene.getStylesheets().add(
                getClass().getResource("app.css").toExternalForm());
		return scene;
        
	}
	
	   
	   public void start () {
			stage.show();
			
	   }
	   
	   protected Scene createSecondGUI() {
		 
		   
		   gp.add(prelbl, 0, 0);
		   gp.add(namelbl, 0, 1);
		   gp.add(emaillbl, 0, 2);
		   gp.add(tellbl, 0, 3);
		   gp.add(addEmail, 2, 2);
		   gp.add(addNumber, 2, 3);
		  
		   
		   gp.add(pretxt, 1, 0);
		   gp.add(nametxt, 1, 1);
		   gp.add(emailtxt, 1, 2);
		   gp.add(teltxt, 1, 3);
		   
		   gp.add(createContact, 2, 0);
           newWindow.setScene(secondScene);

           newWindow.show();
		   
		return scene;
		   		   
	   }

	   protected Scene createThirdGUI() {
           
           String c = tv.getSelectionModel().getSelectedItem();
       	   
       	   vb.getChildren().add(pretxt);
		   vb.getChildren().add(nametxt);
         	
           for(Contact z : App_Model.contactList) {
 			   if(z.getPrename() == c) {
 			   z.getTelArr().size();
 			   for(int i = 0; i < z.getTelArr().size(); i++) {
 				  TextField tf = new TextField();
 				   
 					   tf.setText(z.getTelArr().get(i)); 
 					   txtArrTel.add(i, tf);;
 				  
 				   vb.getChildren().add(tf);
 			   }
 			   }
 		   }
         	
         	for(Contact z : App_Model.contactList) {
  			   if(z.getPrename() == c) {
  			   z.getEmailArr().size();
  			   for(int i = 0; i < z.getEmailArr().size(); i++) {
  				    TextField tf1 = new TextField();
  				    
  					   tf1.setText(z.getEmailArr().get(i));   
 					   txtArrEmail.add(i, tf1);
  				    
  				 
  				    vb.getChildren().add(tf1);
  			   		}
  			   }
  		   }
           vb.getChildren().add(saveChanges);
           
           Stage newWindow = new Stage();
           newWindow.setScene(thirdScene);

           newWindow.show();
		   
	    return scene;
		   
		   
	   }
	   
	   protected void updateTexts() {
	       Translator t = ServiceLocator.getServiceLocator().getTranslator();
	        
	       menuFile.setText(t.getString("program.menu.file"));
	       menuFileLanguage.setText(t.getString("program.menu.file.language"));
           menuHelp.setText(t.getString("program.menu.help"));
           createContact.setText(t.getString("program.button.create.contact"));
           addEmail.setText(t.getString("program.label.add.email"));
           addNumber.setText(t.getString("program.label.add.tel"));
           saveChanges.setText(t.getString("program.button.save.changes"));
           pretxt.setText(t.getString("program.label.pre.txt"));
           nametxt.setText(t.getString("program.label.name.txt"));
           emailtxt.setText(t.getString("program.label.email.txt"));
           teltxt.setText(t.getString("program.label.tel.txt"));
           prelbl.setText(t.getString("program.label.pre.lbl"));
           namelbl.setText(t.getString("program.label.name.lbl"));
           emaillbl.setText(t.getString("program.label.email.lbl"));
           tellbl.setText(t.getString("program.label.tel.lbl"));
           labeli.setText(t.getString("program.label.labeli"));
           
           newWindow.setTitle(t.getString("program.stage.new.window"));
           stage.setTitle(t.getString("program.name"));
	    }
	   
}