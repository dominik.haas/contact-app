package contactApp.appClasses;

import java.util.TreeSet;

import contactApp.Contact;
import contactApp.ServiceLocator;
import contactApp.abstractClasses.Model;


public class App_Model extends Model {
	
	Contact contact;
    ServiceLocator serviceLocator;
    static TreeSet<Contact> contactList = new TreeSet<Contact>();
    
    
	public static void addContacts() {
		
		Contact dominik = new Contact("Dominik", "Haas");
		Contact manuel = new Contact("Manuel","Herzog");
		Contact nico = new Contact("Nico", "Kälin");
		Contact nils = new Contact("Nils","Bürgi");
		Contact lionel = new Contact("Lionel", "Messi");
		Contact erling = new Contact("Erling", "Haaland");
		Contact wayne = new Contact("Wayne", "Rooney");
		
		dominik.addTelToArray("079 900 80 70");
		dominik.addTelToArray("062 500 40 30");
		dominik.addEmailToArray("dominik.haas@students.fhnw.ch");
		dominik.addEmailToArray("dominik.haas@asmiq.ch");
		
		manuel.addTelToArray("078 700 60 50");
		manuel.addEmailToArray("manuel.herzog@students.fhnw.ch");
		
		nico.addTelToArray("077 600 50 40");
		nico.addEmailToArray("nico.kälin@students.fhnw.ch");
		
		nils.addTelToArray("076 500 40 30");
		nils.addEmailToArray("nils.bürgi@students.fhnw.ch");
		
		lionel.addTelToArray("075 33 40 20");
		lionel.addEmailToArray("lionel.messi@barcelona.es");
		
		erling.addTelToArray("062 333 44 55");
		erling.addEmailToArray("erling.haaland@bvb09.de");
		
		wayne.addTelToArray("077 88 22 33");
		wayne.addEmailToArray("wayne.rooney@derby.en");
		
		
		contactList.clear();
		
		contactList.add(dominik);
		contactList.add(nils);
		contactList.add(manuel);
		contactList.add(nico);
		contactList.add(lionel);
		contactList.add(erling);
		contactList.add(wayne);
		
	}
}
